class Main {
	constructor(){
		this.$blockStruct = document.querySelector('.numbers');
		this.$deleteData = document.querySelector('.chancel');
		this.$windowCalc = document.querySelector('.window-in');
		this.sum = 0;
		this.numbers = [];
		this.operation = '';
		this.arrNumber1 = '';

		this.$blockStruct.addEventListener('click',this.clickNumber.bind(this));
		this.$deleteData.addEventListener('click',this.clearFild.bind(this));
	}
	clickNumber(event){
		let clickfild = event.target,
		count = clickfild.dataset.namber;
		if (!isNaN(count)) {
				this.$windowCalc.innerText = '';
				this.arrNumber1 += count;
				this.$windowCalc.innerText = this.arrNumber1;
			} else if (count === '*') {
				this.$windowCalc.innerText = '';
				this.operation = count;
				this.addNewCount();
			} else if (count === '/') {
				this.$windowCalc.innerText = '';
				this.operation = count;
				this.addNewCount();
			} else if (count === '=') {
				this.$windowCalc.innerText = '';
				this.addNewCount();
				this.operationAll();
			} else {
				this.$windowCalc.innerText = '';
				this.operation = count;
				this.addNewCount();
			}
		 		
	}
	addNewCount () {
		this.numbers.push(this.arrNumber1);
		this.arrNumber1 = '';
	}
	resetDate () {
		this.numbers = [];

		this.operation = '';

		this.arrNumber1 = '';
	}
	operationAll(operationCount){
		if(this.numbers.length === 2) {
			this.sum = eval(` ${+this.numbers[0]} ${this.operation} ${+this.numbers[1]}`);
			this.$windowCalc.innerText = this.sum;
			this.resetDate();
		} else {
			return
		}
	}
	clearFild(){
		this.resetDate();
		this.$windowCalc.innerText = '';
	}	
}
document.addEventListener('DOMContentLoaded',()=> {
	 	this.main = new Main();
});